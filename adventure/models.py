from django.db import models
from re import compile

# Create your models here.


class VehicleType(models.Model):
    name = models.CharField(max_length=32)
    max_capacity = models.PositiveIntegerField()

    def __str__(self) -> str:
        return self.name


class Vehicle(models.Model):
    name = models.CharField(max_length=32)
    passengers = models.PositiveIntegerField()
    vehicle_type = models.ForeignKey(VehicleType, null=True, on_delete=models.SET_NULL)
    number_plate = models.CharField(max_length=10)

    def __str__(self) -> str:
        return self.name

    def can_start(self) -> bool:
        return self.vehicle_type.max_capacity >= self.passengers
    
    def get_distribution(self):
        max_capacity = self.vehicle_type.max_capacity
        max_row = int((max_capacity / 2))
        quota   = self.passengers
        
        rows_p = []

        for row in range(max_row):
            if quota > 2:
                rows_p.append([True, True])
            else:
                rows_p.append([True, False])
            
            quota  = quota - 2

        return rows_p
    
    def validate_number_plate(self) -> bool:
        plate = self.number_plate
        validate = True
        if len(plate) != 8 or  "-" not in plate:
            validate = False
        else:
            plate = plate.replace("-", "")
            plate = plate.replace(" ", "")

            plate_format = compile('^[a-zA-Z]{2}[0-9]{2}[0-9]{2}$')
            if plate_format.match(plate) is None:
                validate = False
        
        return validate


class Journey(models.Model):
    vehicle = models.ForeignKey(Vehicle, on_delete=models.PROTECT)
    start = models.DateField()
    end = models.DateField(null=True, blank=True)

    def __str__(self) -> str:
        return f"{self.vehicle.name} ({self.start} - {self.end})"
    
    def is_finished(self) -> bool:
        if self.end is not None:
            return self.start >= self.end
